const String box_name = "mf3";
//const String box_name = "t";

// wifi
#include <WiFi.h>
#include <HTTPClient.h>

const char *ssid = "MiracleFarm_2.4G";  
const char *password = "MadameLingzhi";
//const char *ssid = "pratom_2.4g";  
//const char *password = "ong@0921";

//const char *ssid = "Diablo";  
//const char *password = "123456788";
//const char *ssid = "Sujanya's iPhone";  
//const char *password = "passwordnui";

//PIN
const int UP_TEMP_PIN = 36;
const int DOWN_TEMP_PIN = 25;

const int UP_RH_PIN = 26;
const int DOWN_RH_PIN = 27;

const int UP_C_PIN = 14;
const int DOWN_C_PIN = 13;

const int RELAY_TEMP = 5;
const int RELAY_RH = 18;
const int RELAY_CARBON = 19;

//Temperature & RH% Sensor

#include <DHT.h>
#include <DHT_U.h>

#define DHT_PIN 23
#define DHT_TYPE_1 DHT21   // DHT 22  (AM2302)
#define DHT_TYPE_2 DHT22

DHT dht(DHT_PIN, DHT_TYPE_2);

//Carbon Sensor
#define co2Zero 55

const int CARBON_PIN = 32;

#include <MQ135.h>
#define RZERO 76.63

//OLED
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
#define OLED_RESET     -1 // Reset pin # (or -1 if sharing Arduino reset pin)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

//EEPROM
#include "EEPROM.h"
#define EEPROM_SIZE 100

//Common
#include <ArduinoJson.h>

int box_id = -1;

int calibrate_mode = 0; //0 = None, 1 = Offset, 2 = time
int calibrate_type = 0; //0 = Temperature, 1 = RH, 2 = Carbon

float offset_array[] = {0, 0, 0}; //initial offset Temp, RH, Carbon
float max_array[] = {31, 90, 400}; //Max Temp, RH(stop), Carbon
float min_array[] = {29, 80, 100}; //Min Temp, TH(start), Carbon

bool temp_active = false;
bool rh_active = false;
bool c_active = false;

bool is_online_control = false;
bool is_connected = false;
bool is_connecting = false;
bool is_calibrated = false;
bool is_wait_reconnect = false;
int command_array[] = {0, 0, 0, 0}; //temp, rh, carbon, online

String wifi_dot = "";
const int time_min = 1000*60;
int time_array[] = {time_min, time_min, time_min}; //Operate Time Temp, RH, Carbon

int update_time_count = 0;
const int clock_time = 5000;
const int update_second = 12;
const int update_time = clock_time*update_second;
const int calibrate_time = 500;
const int wifi_timeout = 10000;
int wifi_count = 0;
const int wifi_reconnect_time = update_time;
int wifi_reconnect_count;

int array_count = 30;
int array_index = 0;
float temp_array[] = {
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0
  };
float rh_array[] = {
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0
  };
bool is_array_ready = false;

float smooth_temp = 0;
float smooth_rh = 0;

/*
int temp_count_time = 0;
int rh_count_time = 0;
int carbon_count_time = 0;
int idle_time = 1*1000*60;
int idle_time_count = 0;
const int delay_time = 1000;
*/

void setup() 
{

  Serial.begin(115200);  
  
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(RELAY_TEMP, OUTPUT);
  pinMode(RELAY_RH, OUTPUT);
  pinMode(RELAY_CARBON, OUTPUT);
  
  pinMode(UP_TEMP_PIN,INPUT);
  pinMode(DOWN_TEMP_PIN,INPUT);
  pinMode(UP_RH_PIN,INPUT);
  pinMode(DOWN_RH_PIN,INPUT);
  pinMode(UP_C_PIN,INPUT);
  pinMode(DOWN_C_PIN,INPUT);

  pinMode(CARBON_PIN, INPUT);

  dht.begin();
  EEPROM.begin(EEPROM_SIZE);

  // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // Address 0x3D for 128x64
    Serial.println(F("SSD1306 allocation failed"));
    for(;;); // Don't proceed, loop forever
  }

  displayOLED("Miracle     Farm", 2, 20, 20);

  Serial.println("Setup done");  

  connectWifi();

  delay(2000);

}

void loop() 
{
  
  /*
  Serial.println(String(max_array[0]) + " " +String(max_array[1]) + " " + String(max_array[2]));
  Serial.println(String(min_array[0]) + " " +String(min_array[1]) + " " + String(min_array[2]));
  Serial.println(String(offset_array[0]) + " " +String(offset_array[1]) + " " + String(offset_array[2]));
  Serial.println(" ");*/
  
  if(!is_wait_reconnect) {
  
    if(!is_connected || is_connecting) return;
  
    if(is_connected && WiFi.status() != WL_CONNECTED && !is_connecting) {
      connectWifi();
      return;
    }
    
  } else {
    wifi_reconnect_count += clock_time;
    if(wifi_reconnect_count == wifi_reconnect_time) {
      wifi_reconnect_count = 0;
      connectWifi();
    }
  }

  float temperature = dht.readTemperature();
  float rh = dht.readHumidity();
  int carbon = analogRead(CARBON_PIN);

  if(isnan(temperature) || isnan(rh)) {
    Serial.println("Temp or RH has no data");
    displayOLED("Temp or RH\nhas no data", 1, 0, 0); 
    //TODO SEND nan to server
    delay(clock_time);
    return;
  }
  
  if(array_index == array_count) {

    for(int i=0 ; i < array_count-1 ; i++) {
      temp_array[i] = temp_array[i+1];
      temp_array[array_count-1] = temperature;

      rh_array[i] = rh_array[i+1];
      rh_array[array_count-1] = rh;
    }    
  } else {
    temp_array[array_index] = temperature;
    rh_array[array_index] = rh;
    array_index++;
  }

  smooth_temp = 0;
  smooth_rh = 0;
  //float total_temp = 0;
  //float total_rh = 0;
  for(int i=0 ; i<array_count ; i++) {
    if(temp_array[i] == 0) {
      displayOLED("Pulling data\n"+ String(i) + "/" + String(array_count), 1, 0, 0); 
      Serial.println("Pulling data "+ String(i) + "/" + String(array_count));
      //delay(clock_time/10);
      delay(3000);
      return;
    }
    smooth_temp += temp_array[i];
    smooth_rh += rh_array[i];
  }
  
  //float real_temp = temperature + offset_array[0];
  //float real_rh = rh + offset_array[1];
  float real_temp = (smooth_temp/array_count) + offset_array[0];
  float real_rh = (smooth_rh/array_count) + offset_array[1];
  int real_c = carbon + offset_array[2];   

  //Serial.println("data");
  Serial.println("Raw: "+ String(temperature) + "    smooth: " + String(real_temp));
  //Serial.println(String(rh) + " " + String(real_rh));
  //Serial.println(String(carbon) + " " + String(real_c)); 
  //Serial.println("");
  
  if(update_time_count >= update_time) {  
    //Send data to server
    updateBox(real_temp, real_rh, real_c);
    update_time_count = update_time_count-update_time;    
  }
  
  int up_temp = digitalRead(UP_TEMP_PIN);
  int down_temp = digitalRead(DOWN_TEMP_PIN);
  int up_rh = digitalRead(UP_RH_PIN);
  int down_rh = digitalRead(DOWN_RH_PIN);
  int up_c = digitalRead(UP_C_PIN);
  int down_c = digitalRead(DOWN_C_PIN);
  
  String display_str = "";
  int total_click = 0 + up_temp + down_temp + up_rh + down_rh + up_c + down_c;

  if(is_online_control) {
    Serial.println("Online Control");
    display_str = "Online Control\n\n";
    display_str += getIdleString(2);  
    displayOLED(display_str, 1, 0, 0); 
    setRelay();
    delay(clock_time);
    update_time_count += clock_time;
    return;
  }
  
  bool is_delayed = false;
  
  if( total_click > 0 && total_click < 3 ) {
    
    if(total_click == 2) {
      
      int new_calibrate_type = 0;
      bool is_btn_valid = true;
      
      if(up_temp == 1 && down_temp == 1) {
        //display_str += "Set Temp  "; 
        new_calibrate_type = 0;
      } else if(up_rh == 1 && down_rh == 1) {
        //display_str += "Set RH"; 
        new_calibrate_type = 1;
      } else if(up_c == 1 && down_c == 1) {
        //display_str += "Set CO2"; 
        new_calibrate_type = 2;
      } else {
        is_btn_valid = false;
        display_str = getIdleString(1);
      }

      if(is_btn_valid) {
      
        if(new_calibrate_type == calibrate_type) {
          
          calibrate_mode++;
          
          if(calibrate_mode > 3) {
            calibrate_mode = 0;
            display_str = getIdleString(1);
            //TODO save to ROM
          }
          
        } else {
          if(calibrate_mode == 0) {
            calibrate_mode = 1;
            calibrate_type = new_calibrate_type;
          }
        }
        
        //calibrate_type = new_calibrate_type;
        /*
        if(calibrate_mode == 1) {
          //display_str += "Offset";  
        } else if(calibrate_mode == 2) {
          //display_str += "Max";
        } else if(calibrate_mode == 3) {
          //display_str += "Min";
        } else */
        
      }

      switch(calibrate_mode) {
        case 1:          
        display_str = getOffset();
        break;
        case 2:
        display_str = getMax();
        break;
        case 3:
        display_str = getMin();
        break;
      }
      
    
    } else if(total_click == 1) {

      int input_type = 0;
      int input_dir = 1;
      
      if(up_temp == 1) {
        //display_str += "up temp";  
      } else if(down_temp == 1) {
        //display_str += "down temp";
        input_dir = -1;
      } else if(up_rh == 1) {
        //display_str += "up rh";
        input_type = 1;
      } else if(down_rh == 1) {
        //display_str += "down rh";
        input_dir = -1;
        input_type = 1;
      } else if(up_c == 1) {
        //display_str += "up c";
        input_type = 2;
      } else if(down_c == 1) {
        //display_str += "down c";
        input_dir = -1;
        input_type = 2;
      }

      if(calibrate_mode == 0) {
        display_str = getSetup(input_type);//getIdleString(1);
        //display_str = getIdleString(1);   
      } else {
        if(calibrate_type == input_type) {
          
          switch(calibrate_mode) {
            case 1:
            offset_array[input_type] += input_dir;
            break;
            case 2:
            max_array[input_type] += input_dir;
            break;
            case 3:
            min_array[input_type] += input_dir;
            break;
          }

          is_calibrated = true;
          setRom();
                     
        }

        switch(calibrate_mode) {
           case 1:
            display_str = getOffset();
            break;
            case 2:
            display_str = getMax();
            break;
            case 3:
            display_str = getMin();
            break;
        }
        
      }
      
    }
    
  } else {

    //No input
    if(calibrate_mode == 0) {
      display_str = getIdleString(1);      
    } else if(calibrate_mode == 1) {
      display_str = getOffset();      
    } else if(calibrate_mode == 2) {
      display_str = getMax();      
    } else if(calibrate_mode = 3) {
      display_str = getMin();
    }

    is_delayed = false;
       
  }
  
  if(real_temp >= max_array[0] && !temp_active) {
    temp_active = true; 
    activateRelayId(0, real_temp);
  } else if(real_temp <= min_array[0] && temp_active) {
    temp_active = false;
    activateRelayId(1, real_temp);
  }
  
  if(real_rh <= min_array[1] && !rh_active) {
    rh_active = true; 
    activateRelayId(2, real_rh);
  } else if(real_rh >= max_array[1] && rh_active) {
    rh_active = false;
    activateRelayId(3, real_rh);
  }

  //Serial.println("real: "+ String(real_c) +" max: "+ String(max_array[2])+" min:"+ String(min_array[2]) + " active: "+String(c_active));
   
  if(real_c >= max_array[2] && !c_active) {
    c_active = true; 
    activateRelayId(4, real_c);
  } else if(real_c <= min_array[2] && c_active) {
    c_active = false;
    activateRelayId(5, real_c);
  }

  displayOLED(display_str, 2, 0, 0);

  /*
  Serial.println("Active ");
  Serial.println("T "+String(real_temp) +" max: "+String(max_array[0]) + " min: "+String(min_array[0]) + " active "+String(temp_active));
  Serial.println("R "+String(real_rh) +" max: "+String(max_array[1]) + " min: "+String(min_array[1]) + " active "+String(rh_active));
  Serial.println("C "+String(real_c) +" max: "+String(max_array[2]) + " min: "+String(min_array[2]) + " active "+String(c_active));
  Serial.println(" ");
  */
  display_str += " |||| calibrate mode:";
  display_str += calibrate_mode;
  display_str += " |||| clicks:";
  display_str += total_click;
  display_str += " [";
  if(up_temp > 0) display_str += "up_temp ";
  if(down_temp > 0) display_str += "down_temp ";
  if(up_rh > 0) display_str += "up_rh ";
  if(down_rh > 0) display_str += "down_rh ";
  if(up_c > 0) display_str += "up_c ";
  if(down_c > 0) display_str += "down_c ";
  display_str += "]";

  if(total_click > 0) Serial.println(display_str);
    
  if(is_delayed) {
    delay(calibrate_time);
    update_time_count += calibrate_time;
  } else {
    delay(clock_time);
    update_time_count += clock_time;
  }
  
}

void activateRelayId(int command_id, float value) {

  Serial.println("activateRelayId:" + String(command_id));
  
  String command;

  switch(command_id) {
    case 0:
    digitalWrite(RELAY_TEMP, HIGH);
    command = "temperature_on";
    break;
    case 1:
    digitalWrite(RELAY_TEMP, LOW);
    command = "temperature_off";
    break;
    case 2:
    digitalWrite(RELAY_RH, HIGH);
    command = "rh_on";
    break;
    case 3:
    digitalWrite(RELAY_RH, LOW);
    command = "rh_off";
    break;
    case 4:
    digitalWrite(RELAY_CARBON, HIGH);
    command = "carbon_on";
    break;
    case 5:
    digitalWrite(RELAY_CARBON, LOW);
    command = "carbon_off";
    break;
  }

  if(command != "") updateActivity(command, value);
  
}

String getIdleString(int type) {

  float temperature = smooth_temp/array_count;
  float rh = smooth_rh/array_count;
  
  //float temperature = dht.readTemperature();
  //float rh = dht.readHumidity();
  int carbon  = analogRead(CARBON_PIN);
  
  String display_str = "";

  if(type == 2) {
    if(command_array[0] == 0) {
      display_str += "OFF : ";
    } else {
      display_str += "ON  : ";
    }
  }
  display_str += "TP ";
  display_str += String(temperature + offset_array[0]).substring(0, 4);
  display_str += "C\n";
  
  if(type == 2) {
    if(command_array[1] == 0) {
      display_str += "OFF : ";
    } else {
      display_str += "ON  : ";
    }
  }
  display_str += "RH ";
  display_str += String(rh + offset_array[1]).substring(0, 4);
  display_str += "%\n";
  
  if(type == 2) {
    if(command_array[2] == 0) {
      display_str += "OFF : ";
    } else {
      display_str += "ON  : ";
    }
  }
  display_str += "CO2 ";
  display_str += carbon + offset_array[2]; 
  display_str += "\n";

  if(type == 2) {
    
    display_str += "TP O";
    display_str += offset_array[0];
    display_str += " N";
    display_str += min_array[0];
    display_str += " X";
    display_str += max_array[0];
    
    display_str += "\nRH O";
    display_str += offset_array[1];
    display_str += " N";
    display_str += min_array[1];
    display_str += " X";
    display_str += max_array[1];
      
    display_str += "\nCO O";
    display_str += offset_array[2];   
    display_str += " N";
    display_str += int(min_array[2]);
    display_str += " X";
    display_str += int(max_array[2]);
    
  } else {

    display_str += "T" + String(temp_active);
    display_str += " ";
    display_str += "R" + String(rh_active);
    display_str += " ";
    display_str += "C" + String(c_active);
    
  }
  
  return display_str;
}

String getSetup(int input_type) {

  String display_str = "";

  float real_temp = (smooth_temp/array_count) + offset_array[0];
  float real_rh = (smooth_rh/array_count) + offset_array[1];
  
  switch(input_type) {
    case 0:
    display_str += "Temp ";
    display_str += real_temp;
    display_str += "\n";
    break;
    case 1:
    display_str += "RH ";
    display_str += real_rh;
    display_str += "\n";
    break;
    case 2:
    display_str += "CO2 ";
    display_str += analogRead(CARBON_PIN) + offset_array[2];
    display_str += "\n";
    break;
  }

  display_str += "M ";
  if(input_type < 2) {
    display_str += max_array[input_type];
  } else {
    display_str += int(max_array[input_type]);
  }
  display_str += "\n";
  
  display_str += "N ";
  if(input_type < 2) {
    display_str += min_array[input_type];
  } else {
    display_str += int(min_array[input_type]);
  }
  display_str += "\n";

  display_str += "Off ";
  display_str += offset_array[input_type];
  display_str += "\n";
  
  return display_str;
  
}

String getOffset() {

  String display_str = "Offset\n";
  String prefix;
  String subfix;
  
  switch(calibrate_type) {
    case 0:
    prefix = "Temp\n";
    subfix = " C";
    break;
    case 1:
    prefix = "RH\n";
    subfix = " %";
    break;
    case 2:
    prefix = "CO2\n";
    subfix = " PPM";
    break;
  }

  display_str += prefix;
  display_str += offset_array[calibrate_type];
  display_str += subfix;
  
  return display_str;
  
}

String getMax() {
  
  String display_str = "Max "; 
  String prefix;
  String subfix;
  
  switch(calibrate_type) {
    case 0:
    prefix = "Temp\n";
    subfix = " C";
    break;
    case 1:
    prefix = "RH\n";
    subfix = " %";
    break;
    case 2:
    prefix = "CO2\n";
    subfix = " PPM";
    break;
  }

  display_str += prefix;
  if(calibrate_type < 2) {
    display_str += max_array[calibrate_type];
  } else {
    display_str += int(max_array[calibrate_type]);
  }
  display_str += subfix;
  
  return display_str;
  
}

String getMin() {
  String display_str = "Min "; 
  String prefix;
  String subfix;
  
  switch(calibrate_type) {
    case 0:
    prefix = "Temp  ";
    subfix = " C";
    break;
    case 1:
    prefix = "RH    ";
    subfix = " %";
    break;
    case 2:
    prefix = "CO2   ";
    subfix = " PPM";
    break;
  }

  display_str += prefix;
  if(calibrate_type < 2) {
    display_str += min_array[calibrate_type];
  } else {
    display_str += int(min_array[calibrate_type]);
  }
  display_str += subfix;
  
  return display_str;
}

String getTime() {

  String display_str = "Time ";

  switch(calibrate_type) {
    case 0:
    display_str += "Temp ";
    break;
    case 1:
    display_str += "RH   ";
    break;
    case 2:
    display_str += "CO2  ";
    break;
  }

  display_str += time_array[calibrate_type]/1000/60;
  display_str += " Min";

  return display_str;
  
}

void connectWifi(){

  is_connecting = true;
  
  Serial.println("connectWifi"); 
  
  WiFi.mode(WIFI_OFF);
  delay(1000);
  WiFi.mode(WIFI_STA);
  
  WiFi.begin(ssid, password);

  String display_string;
  display_string = "ConnectingWIFI";
  wifi_dot = "";
  displayOLED(display_string, 2, 0, 20);

  Serial.println(WiFi.status()) ; 
  wifi_count = 0;

  is_wait_reconnect = false;
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    wifi_dot += ".";
    if(wifi_dot.length() > 4) {
      wifi_dot = "";
    }
    
    display_string = "ConnectingWIFI";
    display_string += wifi_dot;
    displayOLED(display_string, 2, 0, 20);

    wifi_count += 500;

    if(wifi_count == wifi_timeout) {
      wifi_count = 0;
      is_wait_reconnect = true;
      break;
    }
    
  }

  if(!is_wait_reconnect) {
  
    display_string = "Connected to\n";
    display_string += ssid;
    display_string += "\nIP\n";
    display_string += WiFi.localIP();
    displayOLED(display_string, 1, 0, 20);
  
    is_connecting = false;
  
    Serial.println("connected") ; 
    Serial.println(WiFi.status()) ; 
      
    delay(2000);
  
    init();

  } else {

    Serial.println("Use ROM setup");
    
    max_array[0] = EEPROM.read(0);
    min_array[0] = EEPROM.read(1);
    offset_array[0] = EEPROM.read(2);

    max_array[1] = EEPROM.read(3);
    min_array[1] = EEPROM.read(4);
    offset_array[1] = EEPROM.read(5);

    String max_carbon = String(EEPROM.read(6)) + String(EEPROM.read(7));
    String min_carbon = String(EEPROM.read(8)) + String(EEPROM.read(9));
    String offset_carbon = String(EEPROM.read(10)) + String(EEPROM.read(11));
    
    max_array[2] = max_carbon.toInt();
    min_array[2] = min_carbon.toInt();
    offset_array[2] = offset_carbon.toInt();
    
  }
  
}

//
const int capacity = JSON_OBJECT_SIZE(3) + 2 * JSON_OBJECT_SIZE(1);
DynamicJsonDocument doc_init(2024*20);
DynamicJsonDocument doc_update(1024*10);
//StaticJsonDocument<capacity> doc;

void updateBox(float temperature, float rh, float carbon) {

  HTTPClient http;

  String postData = "";
  postData += + "[{";
  postData += "\"name\":\""+ box_name +"\"";
  postData += ",\"temperature\":\""+ String(temperature,2) +"\"";
  postData += ",\"relative_humidity\":\""+ String(rh,2) + "\"";
  postData += ",\"carbon\":\"" + String(carbon,2) + "\"";

  //is_calibrated = true;
  //calibrate_type = 2;
  
  if(is_calibrated) {
    
    if(calibrate_type == 0) {
      postData += ",\"temperature_offset\":\"" + String(offset_array[0]) +"\"";
      postData += ",\"temperature_min\":\"" + String(min_array[0]) +"\"";
      postData += ",\"temperature_max\":\"" + String(max_array[0]) +"\"";
    } else if(calibrate_type == 1) {
      postData += ",\"rh_offset\":\"" + String(offset_array[1]) +"\"";
      postData += ",\"rh_min\":\"" + String(min_array[1]) +"\"";
      postData += ",\"rh_max\":\"" + String(max_array[1]) +"\"";
    } else if(calibrate_type == 2) {
      postData += ",\"carbon_offset\":\"" + String(offset_array[2]) +"\"";
      postData += ",\"carbon_min\":\"" + String(min_array[2]) +"\"";
      postData += ",\"carbon_max\":\"" + String(max_array[2]) +"\"";
    }
    
    is_calibrated = false;
      
  }

  postData = postData + "}]";
  
  http.begin("http://ec2-54-169-185-197.ap-southeast-1.compute.amazonaws.com/api/sg/box/update?data="+postData);
  Serial.println("http://ec2-54-169-185-197.ap-southeast-1.compute.amazonaws.com/api/sg/box/update?data="+postData);
  
  int httpCode = http.GET(); 
  String payload = http.getString();  

  http.end();

  Serial.print("updateBox done ");
  Serial.println(httpCode);

  DeserializationError err = deserializeJson(doc_update, payload);
  if (err) {
    Serial.print(F("deserializeJson() failed with code ")); Serial.println(err.c_str());
  }

  JsonArray commandJson = doc_update["data"][0]["box"]["command"].as<JsonArray>();
  onCommand(commandJson);
  
}

void confirmCommand(String confirm_ids) {
  
  HTTPClient http;
  
  http.begin("http://ec2-54-169-185-197.ap-southeast-1.compute.amazonaws.com/api/sg/box/confirmCommand?ids="+confirm_ids);
  //http.addHeader("Content-Type", "application/x-www-form-urlencoded");
  Serial.println("http://ec2-54-169-185-197.ap-southeast-1.compute.amazonaws.com/api/sg/box/confirmCommand?ids="+confirm_ids);

  int httpCode = http.GET(); 
  String payload = http.getString();  

  http.end();

  //Serial.println(confirm_ids);
  Serial.print("confirmCommand done ");
  Serial.println(httpCode);
  //Serial.println(payload);
}

void updateActivity(String command, float value) {

  HTTPClient http;
  
  http.begin("http://ec2-54-169-185-197.ap-southeast-1.compute.amazonaws.com/api/sg/box/updateActivity?name="+box_name+"&command="+command+"&value="+value);
  //http.addHeader("Content-Type", "application/x-www-form-urlencoded");
  Serial.println("http://ec2-54-169-185-197.ap-southeast-1.compute.amazonaws.com/api/sg/box/updateActivity?name="+box_name+"&command="+command+"&value="+value);

  int httpCode = http.GET(); 
  String payload = http.getString();  

  http.end();

  //Serial.println(confirm_ids);
  Serial.print("updateActivity done ");
  Serial.println(httpCode);
  //Serial.println(payload);
  
}

void init() {

  HTTPClient http;
  
  http.begin("http://ec2-54-169-185-197.ap-southeast-1.compute.amazonaws.com/api/sg/box/init?name="+box_name);
  http.addHeader("Content-Type", "application/x-www-form-urlencoded");
  Serial.println("http://ec2-54-169-185-197.ap-southeast-1.compute.amazonaws.com/api/sg/box/init?name="+box_name);

  int httpCode = http.GET(); 
  String payload = http.getString();  
  
  http.end();

  Serial.print("init done ");
  Serial.println(httpCode);
  //Serial.println(payload);

  DeserializationError err = deserializeJson(doc_init, payload);
  if (err) {
    Serial.print(F("deserializeJson() failed with code ")); Serial.println(err.c_str());
  }
  
  String timestamp = doc_init["timestamp"];
  box_id = doc_init["data"]["box"]["id"];
  
  max_array[0] = doc_init["data"]["box"]["temperature_max"];
  max_array[1] = doc_init["data"]["box"]["rh_max"];
  max_array[2] = doc_init["data"]["box"]["carbon_max"];
  
  min_array[0] = doc_init["data"]["box"]["temperature_min"];
  min_array[1] = doc_init["data"]["box"]["rh_min"];
  min_array[2] = doc_init["data"]["box"]["carbon_min"];

  offset_array[0] = doc_init["data"]["box"]["temperature_offset"];
  offset_array[1] = doc_init["data"]["box"]["rh_offset"];
  offset_array[2] = doc_init["data"]["box"]["carbon_offset"];

  Serial.println("init rom ");
  setRom();
  //int offset_array[] = {0, 0, 0}; //initial offset Temp, RH, Carbon
  
  JsonArray commandJson = doc_init["data"]["box"]["command"].as<JsonArray>();
  //onCommand(commandJson);

  command_array[3] = doc_init["data"]["box"]["is_online_control"];
  if(command_array[3] == 1) is_online_control = true;
  
  delay(2000);

  is_connected = true;
  
}

void onCommand(JsonArray commandJson) {
  //Serial.print(commandJson);
  //serializeJsonPretty(commandJson, Serial);

  if(commandJson.size() == 0) return;

  bool is_set_relay = false;
  
  String confirm_ids = "[";
  
  for(int i=0;i<commandJson.size();i++) {
    //serializeJsonPretty(commandJson[i].as<JsonObject>, Serial);
    //JsonObject commandObj = commandJson[i];
  
    String command = commandJson[i].as<JsonObject>()["command"];
    String id = commandJson[i].as<JsonObject>()["id"];
    float value = commandJson[i].as<JsonObject>()["value"];
    //command_array - temp, rh, carbon

    confirm_ids += id;
    if(i < (commandJson.size()-1)) {
      confirm_ids += ",";
    }

    Serial.print("command: ");
    Serial.println(command);
    
    if(command == "temperature_on") {
      command_array[0] = 1;
      is_set_relay = true;
    } else if(command == "temperature_off") {
      command_array[0] = 0;
      is_set_relay = true;
      //is_online_control = true;  
    } else if(command == "rh_on") {
      command_array[1] = 1;
      is_set_relay = true;
    } else if(command == "rh_off") {
      command_array[1] = 0;
      is_set_relay = true;
      //is_online_control = true;  
    } else if(command == "carbon_on") {
      command_array[2] = 1;
      is_set_relay = true;
    } else if(command == "carbon_off") {
      command_array[2] = 0;
      is_set_relay = true;
      //is_online_control = true;
    } else if(command == "leave_box") {
      command_array[0] = 0;
      command_array[1] = 0;
      command_array[2] = 0;
      command_array[3] = 0;
      is_online_control = false;
    } else if(command == "control_box") {
      command_array[3] = 1;
      is_online_control = true;
    }

    //max temp 0, min temp 3, offset temp 6
    
    if(command == "temperature_offset") {
      offset_array[0] = int(value);
    } else if(command == "rh_offset") {
      offset_array[1] = int(value);
    } else if(command == "carbon_offset") {
      offset_array[2] = int(value);
    }

    if(command == "temperature_min") {
      min_array[0] = value;
    } else if(command == "rh_min") {
      min_array[1] = value;
    } else if(command == "carbon_min") {
      min_array[2] = value;
    }

    if(command == "temperature_max") {
      max_array[0] = value;
    } else if(command == "rh_max") {
      max_array[1] = value;
    } else if(command == "carbon_max") {
      max_array[2] = value;
    }

    setRom();
    
  }

  confirm_ids += "]";
  
  int total_on_command = 0;
  for(int i=0;i<4;i++) {
    total_on_command += command_array[i];
  }

  if(is_set_relay) setRelay();    
  
  if(total_on_command > 0) {
    Serial.println("online");
    is_online_control = true;    
  }

  if(commandJson.size() > 0) {
    Serial.println("confirm");
    confirmCommand(confirm_ids);
  }

  Serial.println(total_on_command);
  
}

void setRelay() {

  Serial.print(command_array[0]);
  Serial.print(",");
  Serial.print(command_array[1]);
  Serial.print(",");
  Serial.println(command_array[2]);
  
  if(command_array[0] == 1) {
    digitalWrite(RELAY_TEMP, HIGH);
  } else {
    digitalWrite(RELAY_TEMP, LOW);
  }
  temp_active = bool(command_array[0]);

  if(command_array[1] == 1) {
    digitalWrite(RELAY_RH, HIGH);
  } else {
    digitalWrite(RELAY_RH, LOW);
  }
  rh_active = bool(command_array[1]);

  if(command_array[2] == 1) {
    digitalWrite(RELAY_CARBON, HIGH);
  } else {
    digitalWrite(RELAY_CARBON, LOW);
  }
  c_active = bool(command_array[2]);

  
}

void setRom() {
  
  EEPROM.write(0, max_array[0]);
  EEPROM.write(1, min_array[0]);
  EEPROM.write(2, offset_array[0]);

  EEPROM.write(3, max_array[1]);
  EEPROM.write(4, min_array[1]);
  EEPROM.write(5, offset_array[1]);

  String max_carbon = String(max_array[2]);
  String min_carbon = String(min_array[2]);
  String offset_carbon = String(offset_array[2]);
  
  EEPROM.write(6, max_carbon.substring(0,2).toInt());
  EEPROM.write(7, max_carbon.substring(2,3).toInt());
  EEPROM.write(8, min_carbon.substring(0,2).toInt());
  EEPROM.write(9, min_carbon.substring(2,3).toInt());
  EEPROM.write(10, offset_carbon.substring(0,2).toInt());
  EEPROM.write(11, offset_carbon.substring(2,3).toInt());

  EEPROM.commit();
  
  /*
  Serial.println("rom ");
  Serial.println(String(EEPROM.read(6)) + " " + String(max_array[2]));
  Serial.println(String(EEPROM.read(7)) + " " + String(min_array[2]));
  Serial.println(EEPROM.read(8));
  Serial.println(" ");*/
  
}

void displayOLED(String display_str, int text_size, int x, int y) {

  display_str += " ";
  if(WiFi.status() == WL_CONNECTED) {
    display_str += "W";
  }
  
  display.clearDisplay();
  display.setTextSize(text_size); // Draw 2X-scale text
  display.setTextColor(SSD1306_WHITE);
  display.setCursor(x, y);
  display.println(display_str);
  display.display(); 

  //Serial.println(display_str);
  //Serial.println(" ");

}

/*
void updateOffset() {
  //do when back to calibrate mode 0
  //check all 

  String postData = "";
  //offset_array

  
  HTTPClient http;

  http.begin("http://ec2-54-169-185-197.ap-southeast-1.compute.amazonaws.com/api/sg/box/updateOffset?data="+postData);
  Serial.println("http://ec2-54-169-185-197.ap-southeast-1.compute.amazonaws.com/api/sg/box/updateOffset?data="+postData);

  int httpCode = http.GET(); 
  String payload = http.getString();
  
  http.end();

  Serial.print("updateOffset done ");
  Serial.println(httpCode);
  
}*/
